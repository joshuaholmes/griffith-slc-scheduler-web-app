<?php 

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@home');
Route::get('/about', 'HomeController@about');

Route::get('/user/create', 'UserController@create');
Route::post('user/login', array('as' => 'user.login', 'uses' => 'UserController@login'));
Route::get('user/logout', array('as' => 'user.logout', 'uses' => 'UserController@logout'));
Route::get('user/signin', array('as' => 'user.signin', 'uses' => 'UserController@signin'));


Route::get('speaker/{id}', 'SpeakerController@index');
Route::resource('speaker', 'SpeakerController');

Route::get("test", 'testingController@index');

// Only logged in Users are allowed here:



Route::group(array('middleware' => 'auth'), function()
{
    Route::get('schedule/{schedule_id}', 'UserController@joinActivity');
    Route::get('schedule/{schedule_id}/leave', 'UserController@leaveActivity');
    
    Route::get('user/schedule', array('as' => 'user.schedule', 'uses' => 'UserController@schedule'));
});


// Do these need to be protected by auth? Put in above Route group if yes.
Route::resource('schedule', 'ActivityController');
Route::resource('user', 'UserController');


// Only Admins are allowed here:
Route::group(['middleware' => 'owner'], function () {
    Route::get('admin', array('as' => 'admin.index', 'uses' => 'AdminController@index'));
    Route::get('admin/activities', array('as' => 'admin.activities.index', 'uses' => 'AdminController@activities'));
    Route::get('admin/speakers', array('as' => 'admin.speakers.index', 'uses' => 'AdminController@speakers'));
    Route::get('admin/users', array('as' => 'admin.users.index', 'uses' => 'AdminController@users'));
    Route::get('admin/activity_speaker', array('as' => 'admin.activities.addSpeaker', 'uses' => 'AdminController@activity_speaker'));
    Route::get('admin/users/edit/{id}', 'AdminController@editUser');
    Route::get('admin/activities/edit/{id}', 'ActivityController@edit');
    Route::get('admin/activities/printActivities', 'ActivityController@printActivities'); 
    Route::get('user/schedule/printUserActivities', 'UserController@printUserActivities'); 
    Route::get('admin/activities/printActivityUsers/{id}', 'ActivityController@printActivityUsers'); 
    Route::get('admin/speakers/edit/{id}', 'SpeakerController@edit');
    Route::get('activity_delete/{id}', 'ActivityController@destroy');
    Route::get('activity_delete_user/{activityId}/{userId}', 'ActivityController@deleteUser');
    Route::get('speaker_delete/{id}', 'SpeakerController@destroy');
    Route::get('user_delete/{id}', 'UserController@destroy');
    Route::post('addSpeakerToActivity/{id}', array('as' => 'admin.activities.addSpeakerToActivity', 'uses' => 'ActivityController@addSpeaker'));
    Route::get('deleteSpeakerFromActivity/{activityId}/{SpeakerId}', array('as' => 'admin.activities.deleteSpeakerFromActivity', 'uses' => 'ActivityController@deleteSpeaker'));
});

