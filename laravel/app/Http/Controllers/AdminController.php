<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Activity;
use App\Speaker;
use App\User;
use Auth;
use Session;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.index');
    }
    
    public function activities()
    {
        $activity = Activity::orderBy('startingTime', 'asc')->get();

        return view('admin.activities.index')->withActivities($activity);
    }
    
    public function users()
    {
        $users = User::get();
        return view('admin.users.index')->withUsers($users);
    }
    
    public function editUser($id)
    {
        $user = Auth::user();
        $profile = User::find($id);
        
        return view('admin.users.edit')->withProfile($profile)->withUser($user);
    }
    
    public function speakers()
    {
        $speaker = Speaker::get();

        return view('admin.speakers.index')->withSpeakers($speaker);
    }

    
}
