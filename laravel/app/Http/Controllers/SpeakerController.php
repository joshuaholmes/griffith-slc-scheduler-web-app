<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Speaker;
use Session;
use Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class SpeakerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($id)
    {
        $speaker = Speaker::find($id);
        
        return view('speaker.index')->withSpeaker($speaker);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = \Input::all();
		$v = \Validator::make($input, Speaker::$rules);

		if ($v->passes())
		{
		
		$speaker = new Speaker;
        $speaker->name = $input['name'];
        $speaker->speciality = $input['speciality'];
        $speaker->bio = $input['bio'];
        $speaker->contact = $input['contact'];
        $speaker->avatar = $input['image'];
        $speaker->save();
		
		Session::flash('success', 'Speaker added successfully');
		return Redirect::route('admin.speakers.index');;
		}
		else
		 {
		 // Show validation errors
		 return Redirect::back()->withInput()->withErrors($v);
		 }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $speaker = Speaker::find($id);
        
        return view('admin.speakers.edit')->withSpeaker($speaker)->withUser($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
            $input = \Input::all();
			$v = \Validator::make($input, array('name' => 'required', 'bio' => 'required'));
		
		 if ($v->passes())
		 {
			 $speaker->name = $input['name'];
			 $speaker->speciality = $input['speciality'];
			 $speaker->bio = $input['bio'];
			 $speaker->contact = $input['contact'];
			 $speaker->avatar = $input['image'];
			 $speaker->save();
			 return Redirect::route('admin.speakers.index');
		 }
		 else
		 {
		 // Show validation errors
		 return Redirect::back()->withErrors($v);
		 }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $speaker = Speaker::find($id);

        $speaker->delete();
        
        Session::flash('success', 'Speaker removed successfully');
        return Redirect::route('admin.speakers.index');
    }
}
