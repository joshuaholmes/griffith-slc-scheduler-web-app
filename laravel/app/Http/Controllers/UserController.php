<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\MessageBag;
use Hash;
use Auth;
use App\Activity;
use App\Speaker;
use App\ActivityUser;
use Session;
use Excel;
use App;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use URL;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }
    
    public function schedule()
    {
        $speakers = Speaker::get();
        $user = Auth::user();
        
        $activities = $user->activities()->orderBy('startingTime', 'asc')->get();
        
        return view('user.schedule')->withActivities($activities)->withSpeakers($speakers)->withUser($user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('user.create');
    }
    
    

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = \Input::all();
		$v = \Validator::make($input, User::$rules, User::$messages);

		if ($v->passes())
		{
		$password = $input['password'];
		$encrypted = Hash::make($password);
		
		$user = new User;
		$user->email = $input['email'];
		$user->password = $encrypted;
		$user->name = $input['name'];
		$user->save();
		
		Auth::attempt(array('email' => $input['email'], 'password' => $input['password']));
		
        $user = User::find(Auth::user()->id);
        
        $first = ['startingTime' => '08:00am'];
        $second = ['startingTime' => '09:00am'];
        $third = ['startingTime' => '09:30am'];
        $fourth = ['startingTime' => '10:30am'];
        $fifth = ['startingTime' => '12:30pm'];
        $sixth = ['startingTime' => '3:30pm'];
        $seventh = ['startingTime' => '4:30pm'];

        $activity = Activity::where($first)->orwhere($second)->orwhere($third)->orwhere($fourth)->orwhere($fifth)->orwhere($sixth)->orWhere($seventh)->get();
		
		foreach ($activity as $a) {
			$user->activities()->attach($a);
		}
		
		return Redirect::route('schedule.index');
		}
		else
		 {
		 // Show validation errors
		 return Redirect::back()->withInput()->withErrors($v);
		 }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        	$input = \Input::all();
			$v = \Validator::make($input, array('name' => 'required'));
		
		 if ($v->passes())
		 {
		 	$password = $input['password'];
			$user = User::find($id);
			
			if ($password != "")
			{
				$encrypted = Hash::make($password);
				$user->password = $encrypted;
			}
			
			if ($input['email'] != $user->email)
			{
				$email_validator = \Validator::make($input, array('email' => 'required|email|unique:users'));
				
				if ($email_validator->fails())
					return Redirect::back()->withInput()->withErrors($email_validator->messages());
					
				$user->email = $input['email'];
			}
			
			 $user->name = $input['name'];
			 $user->group = $input['group'];
			 $user->save();
			 return Redirect::route('admin.users.index');
		 }
		 else
		 {
		 // Show validation errors
		 return Redirect::back()->withErrors($v);
		 }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        $user->delete();
        
        Session::flash('success', 'User removed successfully');
        return Redirect::route('admin.users.index');
    }
    
    public function logout()
	{
		Auth::logout();
		Session::flush();
		return Redirect::action('HomeController@home');
	}
	
	public function login()
	{
		$userdata = array(
		'email' => \Input::get('username'),
		'password' => \Input::get('password')
		);
		
		if (Auth::attempt($userdata))
		{
			return Redirect::route('user.schedule');
		}
		else
		{
			Session::put('login_error', 'Uh, oh. Login failed. Try again.');
			return Redirect::to(URL::previous())->withInput();
		}
	}
	
	public function signin()
	{
		return view('user.signin');
	}
	
	 public function joinActivity($activity_id)
    {
        $activity = Activity::find($activity_id);
        
        // Check if already joined activity
        if ( Auth::user()->isAttending($activity) )
        	return Redirect::back()->withError('You already have ' . $activity->name . ' added.');
        
        if ( $activity->full() )
        	return Redirect::back()->withError('The selected activity has no seats left.');
        /* old code
        $au = new ActivityUser;
        $au->activity_id = $activity_id;
        $au->user_id = Auth::user()->id;
        $au->save();
        $success = 'Successfully added '. $activity->name . ' to schedule.';
    	*/
        
        $au   = Activity::find($activity_id);
        $user = User::find(Auth::user()->id);

        $data =  $user->activities()->get();
		
		foreach($data as $d)
		{
			if ($d['startingTime'] ==  $au->startingTime)
				$user->activities()->detach($d);
		}

        $user->activities()->attach($au);
        
		$success = 'Successfully added '. $activity->name . ' to schedule.';
        
        return Redirect::back()->withSuccess($success);
    }
    
    public function leaveActivity($activity_id)
    {
    	$activity = Activity::find($activity_id);
		$user = User::find(Auth::user()->id);				
    	$activity->users()->detach($user);
    	
    	/* old code
    	$au = ActivityUser::whereActivityId($activity->id)->whereUserId(Auth::user()->id)->first();
    	$au->delete();
    	*/
    	
    	$success = "You have left " . $activity->name . ".";
    	
    	return Redirect::back()->withSuccess($success);
    }
    
    public function printUserActivities() {
    	
    	$speakers = Speaker::get();
        $user = Auth::user();
        
        $activities = $user->activities()->orderBy('startingTime', 'asc')->get();
        
        foreach ($activities as $a) 
        {
            $a['speakers'] = $a->speakers()->get();
        }
        
        $user = ['user' => "$user->name"];
        $activity = ['activities' => $activities];
    	
		$pdf = \PDF::loadView('pdf', $user, $activity);
		return $pdf->download('slcschedule.pdf');
    	
    }
}
