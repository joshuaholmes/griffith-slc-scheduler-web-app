<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Activity;
use App\Speaker;
use App\User;
use Session;
use Auth;
use Input;
use Excel;

use App\ActivityUser;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $activities = Activity::orderBy('startingTime', 'asc')->get();
        $speaker = Speaker::get();
        
        // Get the speakers for each activity
        foreach ($activities as $a) 
        {
            $a['speakers'] = $a->speakers()->get();
        }
        
        return view('schedule.index')->withActivities($activities)->withSpeaker($speaker);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('admin.activity.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = \Input::all();
		$v = \Validator::make($input, Activity::$rules);

		if ($v->passes())
		{
		
		$activity = new Activity;
        $activity->startingTime = $input['time'];
        $activity->location = $input['location'];
        $activity->title = $input['title'];
        $activity->desc = $input['description'];
        $activity->seatsAvailable = $input['seats'];
        $activity->save();
		
		Session::flash('success', 'Activity added successfully');
		return Redirect::route('admin.activities.index');
		}
		else
		 {
		 // Show validation errors
		 return Redirect::back()->withInput()->withErrors($v);
		 }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $activity = Activity::with('speakers')->where('id', $id)->first();
        $speakers = Speaker::all();
        $users = $activity->users()->get();
        
        return view('admin.activities.edit')->withActivity($activity)->withUser($user)->withSpeakers($speakers)->withUsers($users);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
            $input = \Input::all();
			$v = \Validator::make($input, array('title' => 'required', 'desc' => 'required', 'startingTime' => 'required', 'location' => 'required', 'seatsAvailable' => 'required'));
		
		 if ($v->passes())
		 {
		    $activity = Activity::find($id);
			$activity->startingTime = $input['startingTime'];
            $activity->location = $input['location'];
            $activity->title = $input['title'];
            $activity->desc = $input['desc'];
            $activity->seatsAvailable = $input['seatsAvailable'];

            /* Old update speaker code
                if (Input::get('speaker') > 0)
    		     {
    		         $activity->speakers()->detach(); // Delete current speaker(s)
    		         $activity->speakers()->attach(Input::get('speaker'));
    		     }
    		     else
    		     {
    		         $activity->speakers()->detach();
    		     }
		     */
		     
		    // New update speaker code
	        $speakers_checked = \Input::get('speaker');    // Get the Ids of selected speakers
            $activity->speakers()->detach();

            if(is_array($speakers_checked))
            {
               foreach($speakers_checked as $speakerId)             // Add speakers to activity one by one
               { 
                   if(!$activity->speakers->contains($speakerId))   // Check to see if speaker already added to the activity
                   {
                       $speaker  = Speaker::find($speakerId); 
                       $speaker->activities()->save($activity);
                   }
               }
            }
		     
		     $activity->save();
            
			 return Redirect::route('admin.activities.index');
		 }
		 else
		 {
		 // Show validation errors
		 return Redirect::back()->withErrors($v);
		 }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $activity = Activity::find($id);

        $activity->delete();
        
        Session::flash('success', 'Activity removed successfully');
        return Redirect::route('admin.activities.index');
    }
    
    public function addSpeaker($id)
    {
        $speakers_checked = \Input::get('selectedSpeakers');    // Get the Ids of selected speakers
        
        if(is_array($speakers_checked))
        {
           foreach($speakers_checked as $speakerId)             // Add speakers to activity one by one
           {
               $activity = Activity::find($id);
               
               if(!$activity->speakers->contains($speakerId))   // Check to see if speaker already added to the activity
               {
                   $speaker  = Speaker::find($speakerId); 
                   $speaker->activities()->save($activity);
               }
               else 
                return "already exists";
           }
        }
        
        Session::flash('success', 'Speaker added to Activity successfully');
        return Redirect::route('admin.activities.addSpeaker');
    }
    
    public function deleteSpeaker($activtyId, $speakerId)
    {
        $activity = Activity::find($activtyId);
        $activity->speakers()->detach($speakerId);
        
        Session::flash('success', 'Speaker removed from Activity successfully');
        return Redirect::route('admin.activities.addSpeaker');
    }
    
    public function deleteUser($activtyId, $userId)
    {
        $activity = Activity::find($activtyId);
        $activity->users()->detach($userId);
        
        Session::flash('success', 'User removed from Activity successfully');
        return Redirect::back();
    }
    
    public function printActivities()
    {
        
        $data = Activity::all()->toArray(); 
        
        Excel::create('Filename', function($excel) use($data) {
            
            $excel->sheet('Sheetname', function($sheet) use($data) {
            $sheet->fromArray($data);
            });
        
        })->download('xls');
        
        return;
        
        
        // Print pdf - code snippet for other controller use.
        $input = \Input::all();
        $html = $input['pdfHtml'];
        
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML("<h1 style='color: red' >Hello</h1>");
        return $pdf->stream( 'allActivities.pdf' );
    }
    
    public function printActivityUsers($activtyId)
    {
        
        $activity = Activity::find($activtyId);
        $data     = $activity->users()->get();
        
        Excel::create('Filename', function($excel) use($data) {
            
            $excel->sheet('Sheetname', function($sheet) use($data) {
            $sheet->fromArray($data);
            });
        
        })->download('xls');
        
        return;
    }
}