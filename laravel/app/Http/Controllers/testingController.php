<?php 
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User as User;
use App\Activity as Activity;
use App\Speaker as Speaker;
class testingController extends Controller {

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function index()
    {
        $data['user']     = User::find(1);
        $data['activity'] = Activity::find(1);
        $data['speaker']  = Speaker::find(1);
        $data['pivot'] = "User::find(1)->activities()->whereRaw('activity_id > 0')->first()";
        return view('testing')->withData($data);
    }

}