<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function home()
	{
		return view('home');
	}
	
	/*
        Documentation function: called to load documentation page.
    */
	public function about()
	{
		return view('about.index');
	}
}
