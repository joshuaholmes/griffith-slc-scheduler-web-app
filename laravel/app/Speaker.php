<?php

namespace App;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Speaker extends Eloquent implements StaplerableInterface {
    use EloquentTrait;
    
    protected $fillable = ['avatar'];
    
    function activities()
    {
        return $this->belongsToMany('App\Activity');
    }
    
    public static $rules = array(
    'name' => 'required',
    'bio' => 'required'
    );
    
     public function __construct(array $attributes = array()) {
        $this->hasAttachedFile('avatar', [
            'styles' => [
                'large' => '400x400',
                'medium' => '300x300',
                'thumb' => '100x100'
            ]
        ]);

        parent::__construct($attributes);
    }
}