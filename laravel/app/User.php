<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];
    
    public static $rules = array(
        'email' => ['required', 'email', 'unique:users', 'regex:/(.*)(griffithuni)|(griffith)\.edu\.au$/i'],
        'confirm_email' => 'required|same:email',
        'password' => 'required|min:8',
        'confirm_password' => 'required|same:password',
        'name' => 'required'
    );
    
    public static $messages = array(
        'email.regex' => 'You must have a valid Griffith email address.',
   );

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
    
    function activities()
    {
        return $this->belongsToMany('App\Activity')->withTimestamps();
    }
    
    public function isAttending(Activity $activity)
    {
        return $this->belongsToMany('App\Activity')->whereActivityId($activity->id)->count() > 0;
    }
}
