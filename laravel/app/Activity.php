<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Activity extends Eloquent {
    
    function users()
    {
        return $this->belongsToMany('App\User')->withTimestamps();
    }
    
    function speakers()
    {
        return $this->belongsToMany('App\Speaker');
    }
    
    function speaker() // Get ONE speaker
    {
        return $this->belongsTo('App\Speaker');
    }
    
    function seatsTaken()
    {
        return $this->belongsToMany('App\User')->count();
    }
    
    function seatsLeft()
    {
        return $this->seatsAvailable - $this->seatsTaken();
    }
    
    function full()
    {
        return $this->seatsTaken() >= $this->seatsAvailable;
    }
    
    public static $rules = array(
    'title' => 'required',
    'description' => 'required',
    'location' => 'required',
    'seats' => 'required',
    'time' => 'required'
    );
    
}