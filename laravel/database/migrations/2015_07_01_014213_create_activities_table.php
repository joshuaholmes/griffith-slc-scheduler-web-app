<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /* This migration is used to create the activities table in the database*/
        
        Schema::create('activities', function($table)
        {
            $table->increments('id');
            $table->enum('startingTime', ['08:00am', '09:00am', '09:30am', '10:30am', '11:00am', '12:30pm', '1:30pm', '2:30pm', '3:30pm', '4:30pm']);
            $table->string('location');
            $table->string('title');
            $table->string('desc');
            $table->integer('seatsAvailable');
            $table->rememberToken();
            $table->timestamps();
            
            //$table->integer('activities_speakers_id'); // May not be neccessary with the eloquent model
            //$table->integer('activities_users_id');    // May not be neccessary with the eloquent model
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('activities');
    }
}
