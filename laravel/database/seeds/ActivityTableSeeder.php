<?php

use App\Activity;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ActivityTableSeeder extends Seeder 
{
    public function run()
    {
        $activity = new Activity;
        $activity->startingTime = 10;
        $activity->location = "G. 27";
        $activity->title = "Test Activity";
        $activity->desc = "This is a test activity which will be used for the testing the functionality of the app";
        $activity->seatsAvailable = 10;
        $activity->save();
    }
}