<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UserTableSeeder extends Seeder 
{
    public function run()
    {
        $user = new User;
        $user->name = "Joshua Holmes";
        $user->email = "joshua.holmes@griffithuni.edu.au";
        $user->password =  Hash::make('Password123');
        $user->group = "Admin";
        $user->save();
    }
}