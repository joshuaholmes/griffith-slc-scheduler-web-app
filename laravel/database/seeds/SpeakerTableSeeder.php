<?php

use App\Speaker;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class SpeakerTableSeeder extends Seeder 
{
    public function run()
    {
        $speaker = new Speaker;
        $speaker->name = "testSpeaker";
        $speaker->speciality = "testingLeadership";
        $speaker->bio = "I am a test who is a leader";
        $speaker->contact = "0410101001";
        $speaker->save();
    }
}