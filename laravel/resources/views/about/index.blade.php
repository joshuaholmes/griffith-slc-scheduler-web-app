@extends('master')

@section('header')
    
    @stop

@section('main')
  <div class="mdl-cell mdl-cell--2-col mdl-cell--hide-tablet mdl-cell--hide-phone"></div>
    <div class="demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--8-col">
      <h3>About</h3>
      <p>The Griffith Student Leadership Conference (SLC) Scheduler web app was designed and developed to make it easier to plan for and attend the Griffith Student Leadership Conference.<br><br>
      The amazing team behind this website are <a href = "http://joshuaholmes.id.au" target="_blank">Joshua Holmes</a>, <a href="http://chiragchoudhary.com/">Chirag Choudhary</a> and <a href="https://au.linkedin.com/in/theshaw">Thomas Shaw</a>, all Griffith University School of ICT students and freelance web design and developers.<br>
      This website is built using Laravel and Material Design Light, as well as DOMPDF Wrapper and Laravel Excel v2 plugins.
      
      <h3>Contact Us</h3>
      We welcome your feedback and request you report any problems or bugs you find to <a href="mailto:bugs@slcgriffith.com.au?Subject=I%20found%20a%20bug!">bugs@slcgriffith.com.au</a> and direct all other feedback, including feature requests or suggestions, to <a href ="mailto:hello@slcgriffith.com.au?Subject=Hello">hello@slcgriffith.com.au</a><br>
      <br>
            
      <h3>Terms of Use ("Terms")</h3>
      <p>Last updated: July 31, 2015</p>
      
      <p>Please read these Terms and Conditions ("Terms", "Terms and Conditions") carefully before using the www.slcgriffith.com.au website (the "Service") operated by the Griffith Student Leadership Conference ("us", "we", or "our").</p>
      
      <p>Your access to and use of the Service is conditioned on your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who access or use the Service.</p>
      
      <p>By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you may not access the Service.</p>
      
      <p><strong>Accounts</strong></p>
      
      <p>When you create an account with us, you must provide us information that is accurate, complete, and current at all times. Failure to do so constitutes a breach of the Terms, which may result in immediate termination of your account on our Service.</p>
      
      <p>You are responsible for safeguarding the password that you use to access the Service and for any activities or actions under your password, whether your password is with our Service or a third-party service.</p>
      
      <p>You agree not to disclose your password to any third party. You must notify us immediately upon becoming aware of any breach of security or unauthorized use of your account.</p>    
      
      <p><strong>Links To Other Web Sites</strong></p>
      
      <p>Our Service may contain links to third-party web sites or services that are not owned or controlled by the Griffith Student Leadership Conference.</p>
      
      <p>The Griffith Student Leadership Conference has no control over, and assumes no responsibility for, the content, privacy policies, or practices of any third party web sites or services. You further acknowledge and agree that the Griffith Student Leadership Conference shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such web sites or services.</p>
      
      <p>We strongly advise you to read the terms and conditions and privacy policies of any third-party web sites or services that you visit.</p>
      
      <p><strong>Termination</strong></p>
      
      <p>We may terminate or suspend access to our Service immediately, without prior notice or liability, for any reason whatsoever, including without limitation if you breach the Terms.</p>
      
      <p>All provisions of the Terms which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability.</p>
      
      <p>We may terminate or suspend your account immediately, without prior notice or liability, for any reason whatsoever, including without limitation if you breach the Terms.</p>
      
      <p>Upon termination, your right to use the Service will immediately cease. If you wish to terminate your account, you may simply discontinue using the Service.</p>
      
      <p>All provisions of the Terms which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability.</p>
      
      <p><strong>Governing Law</strong></p>
      
      <p>These Terms shall be governed and construed in accordance with the laws of Queensland, Australia, without regard to its conflict of law provisions.</p>
      
      <p>Our failure to enforce any right or provision of these Terms will not be considered a waiver of those rights. If any provision of these Terms is held to be invalid or unenforceable by a court, the remaining provisions of these Terms will remain in effect. These Terms constitute the entire agreement between us regarding our Service, and supersede and replace any prior agreements we might have between us regarding the Service.</p>
      
      <p><strong>Changes</strong></p>
      
      <p>We reserve the right, at our sole discretion, to modify or replace these Terms at any time. If a revision is material we will try to provide at least 30 days notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion.</p>
      
      <p>By continuing to access or use our Service after those revisions become effective, you agree to be bound by the revised terms. If you do not agree to the new terms, please stop using the Service.</p>
      
      <p><strong>Contact Us</strong></p>
      
      <p>If you have any questions about these Terms, please contact us.</p><br>
      
      <h3>Privacy Policy</h3>
      
      <p>Last updated: July 31, 2015</p>
      
      <p>Griffith SLC Scheduler ("us", "we", or "our") operates the www.slcgriffith.com.au website (the "Service").</p>
      
      <p>This page informs you of our policies regarding the collection, use and disclosure of Personal Information when you use our Service.</p>
      
      <p>We will not use or share your information with anyone except as described in this Privacy Policy.</p>
      
      <p>We use your Personal Information for providing and improving the Service. By using the Service, you agree to the collection and use of information in accordance with this policy. Unless otherwise defined in this Privacy Policy, terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, accessible at www.slcgriffith.com.au</p>
      
      <p><strong>Information Collection And Use</strong></p>
      
      <p>While using our Service, we may ask you to provide us with certain personally identifiable information that can be used to contact or identify you. Personally identifiable information may include, but is not limited to, your name and email address ("Personal Information").</p>
      
      <p>Griffith is committed to protecting the privacy of students’ personal information. 
      
      Griffith University collects, stores and uses personal information gathered here only for the purposes of coordinating Mentoring@Griffith programs and events. The information collected is confidential and will not be disclosed to third parties without your consent, except to meet government, legal or other regulatory authority requirements. For further information consult the University’s Privacy Plan at http://www.griffith.edu.au/about-griffith/plans-publications/griffith-university-privacy-plan</p>
      
      <p><strong>Log Data</strong></p>
      
      <p>We collect information that your browser sends whenever you visit our Service ("Log Data"). This Log Data may include information such as your computer's Internet Protocol ("IP") address, browser type, browser version, the pages of our Service that you visit, the time and date of your visit, the time spent on those pages and other statistics.</p>
      
      <p><strong>Cookies</strong></p>
      
      <p>Cookies are files with small amount of data, which may include an anonymous unique identifier. Cookies are sent to your browser from a web site and stored on your computer's hard drive.</p>
      
      <p>We use "cookies" to collect information. You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent. However, if you do not accept cookies, you may not be able to use some portions of our Service.</p>
      
      <p><strong>Security</strong></p>
      
      <p>The security of your Personal Information is important to us, but remember that no method of transmission over the Internet, or method of electronic storage is 100% secure. While we strive to use commercially acceptable means to protect your Personal Information, we cannot guarantee its absolute security.</p>
      
      <p><strong>Links To Other Sites</strong></p>
      
      <p>Our Service may contain links to other sites that are not operated by us. If you click on a third party link, you will be directed to that third party's site. We strongly advise you to review the Privacy Policy of every site you visit.</p>
      
      <p>We have no control over, and assume no responsibility for the content, privacy policies or practices of any third party sites or services.</p>
      
      <p><strong>Children's Privacy</strong></p>
      
      <p>Our Service does not address anyone under the age of 13 ("Children").</p>
      
      <p>We do not knowingly collect personally identifiable information from children under 13. If you are a parent or guardian and you are aware that your Children has provided us with Personal Information, please contact us. If we discover that a Children under 13 has provided us with Personal Information, we will delete such information from our servers immediately.</p>
      
      <p><strong>Changes To This Privacy Policy</strong></p>
      
      <p>We may update our Privacy Policy from time to time. We will notify you of any changes by posting the new Privacy Policy on this page.</p>
      
      <p>You are advised to review this Privacy Policy periodically for any changes. Changes to this Privacy Policy are effective when they are posted on this page.</p>
      
      <p><strong>Contact Us</strong></p>
      
      <p>If you have any questions about this Privacy Policy, please contact us.</p>
    </div>
          
    @section('footer')
    @stop
  @stop