@extends('master')

@section('header')
    <style>
.full-width {
    width: 100%;
}
</style>
    
    @stop

@section('main')
<div class="mdl-cell mdl-cell--2-col mdl-cell--hide-tablet mdl-cell--hide-phone"></div>
          <div class="demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--8-col">
            <?php $error = Session::get('error'); ?>
@if ($error)
  <p>{!! $error !!}</p>
  <br><br>
@endif
            <h3>Full schedule</h3>
            <p>Click for details.</p>
            
              <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp mdl-cell--12-col full-width">
  <thead>
    <tr>
      <h5>8:00am</h5>
      <th class="mdl-data-table__cell--non-numeric full-width">Session Title</th>
      <th>Location</th>
    </tr>
  </thead>
  <tbody>
     @foreach($activities as $activity)
     @if ($activity->startingTime == '08:00am')
    <tr>
      <td class="mdl-data-table__cell--non-numeric full-width">{{$activity->title}}</td>
      <td>{{$activity->location}}</td>
    </tr>
    @endif
    @endforeach
  </tbody>
</table>

 <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp mdl-cell--12-col full-width">
  <thead>
    <tr>
      <h5>9:00am</h5>
      <th class="mdl-data-table__cell--non-numeric full-width">Session Title</th>
      <th>Description</th>
      <th>Speaker</th>
      <th>Location</th>
    </tr>
  </thead>
  <tbody>
     @foreach($activities as $activity)
     @if ($activity->startingTime == '09:00am')
    <tr>
      <td class="mdl-data-table__cell--non-numeric full-width">{{$activity->title}}</td>
      <td>{{$activity->desc}}</td>
      <td>@foreach ($activity['speakers'] as $s) <a href="/laravel/public/speaker/{{$s->id}}">{{$s->name}}</a> @endforeach</td>
      <td>{{$activity->location}}</td>
    </tr>
    @endif
    @endforeach
  </tbody>
</table>
 <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp mdl-cell--12-col full-width">
  <thead>
    <tr>
      <h5>9:30am</h5>
      <th class="mdl-data-table__cell--non-numeric full-width">Session Title</th>
      <th>Description</th>
      <th>Speaker</th>
      <th>Location</th>
    </tr>
  </thead>
  <tbody>
     @foreach($activities as $activity)
     @if ($activity->startingTime == '09:30am')
    <tr>
      <td class="mdl-data-table__cell--non-numeric full-width">{{$activity->title}}</td>
      <td>{{$activity->desc}}</td>
      <td>@foreach ($activity['speakers'] as $s) <a href="/laravel/public/speaker/{{$s->id}}">{{$s->name}}</a> @endforeach</td>
      <td>{{$activity->location}}</td>
    </tr>
    @endif
    @endforeach
  </tbody>
</table>
 <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp mdl-cell--12-col full-width">
  <thead>
    <tr>
      <h5>10:30am</h5>
      <th class="mdl-data-table__cell--non-numeric full-width">Session Title</th>
      <th>Location</th>
    </tr>
  </thead>
  <tbody>
     @foreach($activities as $activity)
     @if ($activity->startingTime == '10:30am')
    <tr>
      <td class="mdl-data-table__cell--non-numeric full-width">{{$activity->title}}</td>
      <td>{{$activity->location}}</td>
    </tr>
    @endif
    @endforeach
  </tbody>
</table>
 <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp mdl-cell--12-col full-width">
  <thead>
    <tr>
      <h5>11:00am</h5>
      <p><i>Make sure you select one to guarantee your spot.</i></p>
      <th></th>
      <th class="mdl-data-table__cell--non-numeric full-width">Session Title</th>
      <th>Description</th>
      <th>Speaker</th>
      <th>Location</th>
      <th>Space</th>
    </tr>
  </thead>
  <tbody>
     @foreach($activities as $activity)
     @if ($activity->startingTime == '11:00am')
    <tr>
      <td>
      @if ( Auth::check() && Auth::user()->isAttending($activity) )
        <a href="{!! secure_url('schedule/'.$activity->id.'/leave') !!}">Leave</a>
      @elseif (Auth::check() && !Auth::user()->isAttending($activity) )
        <a href="{!! secure_url('schedule/'.$activity->id) !!}">Join</a>
      @elseif (Auth::check() && !Auth::user()->isAttending($activity) && $activity->full())
        Full
      @endif
      @if ( $activity->full() ) (Full)@endif
      </td>
      <td class="mdl-data-table__cell--non-numeric full-width">{{$activity->title}}</td>
      <td>{{$activity->desc}}</td>
      <td>@foreach ($activity['speakers'] as $s) <a href="/laravel/public/speaker/{{$s->id}}">{{$s->name}}</a> @endforeach</td>
      <td>{{$activity->location}}</td>
      <td>{{$activity->seatsAvailable}}</td>
    </tr>
    @endif
    @endforeach
  </tbody>
</table>
 <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp mdl-cell--12-col full-width">
  <thead>
    <tr>
      <h5>12:30pm</h5>
      <th class="mdl-data-table__cell--non-numeric full-width">Session Title</th>
      <th>Description</th>
      <th>Location</th>
    </tr>
  </thead>
  <tbody>
     @foreach($activities as $activity)
     @if ($activity->startingTime == '12:30pm')
    <tr>
      <td class="mdl-data-table__cell--non-numeric full-width">{{$activity->title}}</td>
      <td>{{$activity->desc}}</td>
      <td>{{$activity->location}}</td>
    </tr>
    @endif
    @endforeach
  </tbody>
</table>
 <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp mdl-cell--12-col full-width">
  <thead>
    <tr>
      <h5>1:30pm</h5>
      <p><i>Make sure you select one to guarantee your spot.</i></p>
      <th></th>
      <th class="mdl-data-table__cell--non-numeric full-width">Session Title</th>
      <th>Description</th>
      <th>Speaker</th>
      <th>Location</th>
      <th>Space</th>
    </tr>
  </thead>
  <tbody>
     @foreach($activities as $activity)
     @if ($activity->startingTime == '1:30pm')
    <tr>
      <td>
      @if ( Auth::check() && Auth::user()->isAttending($activity) )
        <a href="{!! secure_url('schedule/'.$activity->id.'/leave') !!}">Leave</a>
      @elseif (Auth::check() && !Auth::user()->isAttending($activity) )
        <a href="{!! secure_url('schedule/'.$activity->id) !!}">Join</a>
      @endif
      @if ( $activity->full() ) (Full)@endif
      </td>
      <td class="mdl-data-table__cell--non-numeric full-width">{{$activity->title}}</td>
      <td>{{$activity->desc}}</td>
      <td>@foreach ($activity['speakers'] as $s) <a href="/laravel/public/speaker/{{$s->id}}">{{$s->name}}</a> @endforeach</td>
      <td>{{$activity->location}}</td>
      <td>{{$activity->seatsAvailable}}</td>
    </tr>
    @endif
    @endforeach
  </tbody>
</table>
 <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp mdl-cell--12-col full-width">
  <thead>
    <tr>
      <h5>2:30pm</h5>
      <p><i>Make sure you select one to guarantee your spot.</i></p>
      <th></th>
      <th class="mdl-data-table__cell--non-numeric full-width">Session Title</th>
      <th>Description</th>
      <th>Speaker</th>
      <th>Location</th>
      <th>Space</th>
    </tr>
  </thead>
  <tbody>
     @foreach($activities as $activity)
     @if ($activity->startingTime == '2:30pm' && (Auth::check() && !Auth::user()->isAttending($activity) ) )
    <tr>
      <td>
      @if ( Auth::check() && Auth::user()->isAttending($activity) )
        <a href="{!! secure_url('schedule/'.$activity->id.'/leave') !!}">Leave</a>
      @elseif (Auth::check() && !Auth::user()->isAttending($activity) )
        <a href="{!! secure_url('schedule/'.$activity->id) !!}">Join</a>
      @endif
      @if ( $activity->full() ) (Full)@endif
      </td>
      <td class="mdl-data-table__cell--non-numeric full-width">{{$activity->title}}</td>
      <td>{{$activity->desc}}</td>
      <td>@foreach ($activity['speakers'] as $s) <a href="/laravel/public/speaker/{{$s->id}}">{{$s->name}}</a> @endforeach</td>
      <td>{{$activity->location}}</td>
      <td>{{$activity->seatsAvailable}}</td>
    </tr>
    @endif
    @endforeach
  </tbody>
</table>
 <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp mdl-cell--12-col full-width">
  <thead>
    <tr>
      <h5>3:30pm</h5>
      <th class="mdl-data-table__cell--non-numeric full-width">Session Title</th>
      <th>Description</th>
      <th>Speaker</th>
      <th>Location</th>
    </tr>
  </thead>
  <tbody>
     @foreach($activities as $activity)
     @if ($activity->startingTime == '3:30pm')
    <tr>
      <td class="mdl-data-table__cell--non-numeric full-width">{{$activity->title}}</td>
      <td>{{$activity->desc}}</td>
      <td>@foreach ($activity['speakers'] as $s) <a href="/laravel/public/speaker/{{$s->id}}">{{$s->name}}</a> @endforeach</td>
      <td>{{$activity->location}}</td>
    </tr>
    @endif
    @endforeach
  </tbody>
</table>
 <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp mdl-cell--12-col full-width">
  <thead>
    <tr>
      <h5>4:30pm</h5>
      <th class="mdl-data-table__cell--non-numeric full-width">Session Title</th>
      <th>Description</th>
      <th>Speaker</th>
      <th>Location</th>
    </tr>
  </thead>
  <tbody>
     @foreach($activities as $activity)
     @if ($activity->startingTime == '4:30pm')
    <tr>
      <td class="mdl-data-table__cell--non-numeric full-width">{{$activity->title}}</td>
      <td>{{$activity->desc}}</td>
      <td>@foreach ($activity['speakers'] as $s) <a href="/laravel/public/speaker/{{$s->id}}">{{$s->name}}</a> @endforeach</td>
      <td>{{$activity->location}}</td>
    </tr>
    @endif
    @endforeach
  </tbody>
</table>

              
            </div>

          @section('footer')
          <footer class="demo-footer mdl-mini-footer">
          <div class="mdl-mini-footer--left-section">
            <ul class="mdl-mini-footer--link-list">
              <li><a href="#">Help</a></li>
              <li><a href="#">Privacy and Terms</a></li>
              <li><a href="#">User Agreement</a></li>
            </ul>
          </div>
        </footer>
          @stop
          
          
 @stop
 
  @section('modal')
 
  @stop