@extends('master')

@section('header')

    <style>
  .demo-card-wide.mdl-card {
    width: 512px;
  }
  .demo-card-wide > .mdl-card__title {
    color: #fff;
    height: 176px;
    background: url('https://lh3.googleusercontent.com/TT-dpPiR8U_EhkoHpglhLIylzqFgpovTi73RpDeLQzU=w533-h400-no') center / cover;
  }
  .demo-card-wide > .mdl-card__menu {
    color: #fff;
  }
    .demo-card-square.mdl-card {
    width: 320px;
    height: 320px;
  }
  .demo-card-square > .mdl-card__title {
    color: #fff;
    background:
      url('https://lh3.googleusercontent.com/tFRjq5nyrNmpSyF3UNb7cBkk1zDioKR7gcldcKSU_aw=w355-h215-no') bottom right 15% no-repeat #46B6AC;
  }
</style>
    @stop

@section('main')
<div class="mdl-cell mdl-cell--4-col mdl-cell--hide-tablet mdl-cell--hide-phone"></div>

<div class="mdl-card mdl-shadow--2dp demo-card-wide mdl-cell mdl-cell--6-col">
  <div class="mdl-card__title">
    <h2 class="mdl-card__title-text">Welcome</h2>
  </div>
  <div class="mdl-card__supporting-text">
    The Griffith Student Leadership Conference (SLC) Scheduler is a place for you to add the presentations and workshops you wish to attend. You can then view the personalised schedule on your phone or print a PDF version.
  </div>
  <div class="mdl-card__actions mdl-card--border">
    <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
      Get Started
    </a>
  </div>
</div>


@stop