@extends('master')

@section('header')

    
    @stop

@section('main')
<div class="mdl-cell mdl-cell--2-col mdl-cell--hide-tablet mdl-cell--hide-phone"></div>
          <div class="demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--8-col">
            <h3>Sign in</h3>
            
            {!! Form::open(array('url' => secure_url('user/login'))) !!}
            <div class="form-group">
                                  <div class="mdl-textfield mdl-js-textfield textfield-demo mdl-textfield--floating-label">
                        		        {!! Form::text('username', null, array('class' => 'mdl-textfield__input')) !!}
                        		        {!! Form::label('username', 'Username', array('class' => 'mdl-textfield__label')) !!}
                        		    </div>
                        		    </div>
                        		<div class="form-group">
                        		  <div class="mdl-textfield mdl-js-textfield textfield-demo mdl-textfield--floating-label">
                            		    {!! Form::password('password', array('class' => 'mdl-textfield__input')) !!}
                            		    {!! Form::label('password', 'Password', array('class' => 'mdl-textfield__label')) !!}
                            		    </div>
                        		</div>
                        		<div align="right">
                        		{!! Form::submit('Login', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent']) !!}
                        		</div>
                        		{!! Session::get('login_error') !!}
                        		{!! Form::close() !!}
          </div>
          
          @stop