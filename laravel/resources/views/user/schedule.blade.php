@extends('master')

@section('header')
    <style>
 .demo-card-wide.mdl-card {
    width: 512px;
  }
  .demo-card-wide > .mdl-card__title {
    color: #fff;
    height: 176px;
    background: url('https://lh3.googleusercontent.com/TT-dpPiR8U_EhkoHpglhLIylzqFgpovTi73RpDeLQzU=w533-h400-no') center / cover;
  }
  .demo-card-wide > .mdl-card__menu {
    color: #fff;
  }
</style>
    
    @stop

@section('main')
<div class="mdl-cell mdl-cell--2-col mdl-cell--hide-tablet mdl-cell--hide-phone"></div>
          <div class="demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--8-col">
            <h3>Personal schedule for {{$user->name}}</h3>
            <p><b>Don't forget to add the 11:00am, 1:30pm and 2:30pm sessions from the full schedule!</b></p>
            <p>Click for details.</p>
            {!! Form::open(array('url' => url('user/schedule/printUserActivities'), 'method' => 'get', 'id' => 'pdfForm')) !!}
  {!! Form::submit('Print PDF', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--accent mdl-color-text--accent-contrast', 'id' => 'view-source', 'onclick' => 'printPDF()', 'target' => '_blank' ]) !!}
{!! Form::close() !!}<br>
              <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp mdl-cell--12-col full-width">
  <thead>
    <tr>
      <th>Time</th>
      <th class="mdl-data-table__cell--non-numeric full-width">Title</th>
      <th>Description</th>
      <th>Speaker</th>
      <th>Room</th>
    </tr>
  </thead>
  <tbody>
     @foreach($activities as $activity)
    <tr>
      <td>{{$activity->startingTime}}</td>
      <td class="mdl-data-table__cell--non-numeric full-width">{{$activity->title}}</td>
      <td>{{$activity->desc}}</td>
      <td>@foreach ($activity['speakers'] as $s) <a href="/laravel/public/speaker/{{$s->id}}">{{$s->name}}</a> @endforeach</td>
      <td>{{$activity->location}}</td>
    </tr>
    @endforeach
  </tbody>
</table>


              
            </div>

  

   
          @stop
          
@section('button')



@stop