@extends('master')

@section('header')

    
    @stop

@section('main')
<div class="mdl-cell mdl-cell--2-col mdl-cell--hide-tablet mdl-cell--hide-phone"></div>
          <div class="demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--8-col">
            <h3>Sign up</h3>
            <p>Personalise your schedule in 30 seconds. We just need your Griffith email address, name and a password.</p>
            {!! Form::open(array('url' => secure_url('user'))) !!}
                @if ($errors->has())
                                            <div class="alert alert-danger">
                                                @foreach ($errors->all() as $error)
                                                    {{ $error }}<br>       
                                                @endforeach
                                            </div>
                                        @endif
                                        
                                        <div class="form-group">
                                            <div class="mdl-textfield mdl-js-textfield textfield-demo mdl-textfield--floating-label @if ($errors->has('name')) has-error @endif">
                                                {!! Form::text('name', null, array('class' => 'mdl-textfield__input')) !!}
                                                {!! Form::label('name', 'Your name', array('class' => 'mdl-textfield__label')) !!}
                                            </div>
                                            </div>
                                        @if ($errors->has('name')) <span class="label label-danger">{{ $errors->first('name') }}</span> @endif
                                        <div class="form-group">
                                                <div class="mdl-textfield mdl-js-textfield textfield-demo mdl-textfield--floating-label @if ($errors->has('email')) has-error @endif">
                                                    {!! Form::email('email', null, array('class' => 'mdl-textfield__input')) !!}
                                                    {!! Form::label('email', 'Your Griffith email', array('class' => 'mdl-textfield__label')) !!}
                                                </div>
                                                </div>
                                        @if ($errors->has('email')) <span class="label label-danger">{{ $errors->first('email') }}</span> @endif
<div class="form-group">
                                                <div class="mdl-textfield mdl-js-textfield textfield-demo mdl-textfield--floating-label @if ($errors->has('confirm_email')) has-error @endif">
                                                    {!! Form::email('confirm_email', null, array('class' => 'mdl-textfield__input')) !!}
                                                    {!! Form::label('confirm_email', 'Your Griffith email again', array('class' => 'mdl-textfield__label')) !!}
                                                </div>
                                                </div>
                                        @if ($errors->has('confirm_email')) <span class="label label-danger">{{ $errors->first('confirm_email') }}</span> @endif
                <div class="form-group">
                                            <div class="mdl-textfield mdl-js-textfield textfield-demo mdl-textfield--floating-label @if ($errors->has('password')) has-error @endif">
                                                {!! Form::password('password', array('class' => 'mdl-textfield__input')) !!}
                                                {!! Form::label('password', 'Pick a password of at least 8 characters', array('class' => 'mdl-textfield__label')) !!}
                                            </div>
                                            </div>
                                        @if ($errors->has('password')) <span class="label label-danger">{{ $errors->first('password') }}</span> @endif
                                        <div class="form-group">
                                            <div class="mdl-textfield mdl-js-textfield textfield-demo mdl-textfield--floating-label @if ($errors->has('confirm_password')) has-error @endif">
                                                {!! Form::password('confirm_password', array('class' => 'mdl-textfield__input')) !!}
                                                {!! Form::label('confirm_password', 'Your password again', array('class' => 'mdl-textfield__label')) !!}
                                            </div>
                                            </div>
                                        @if ($errors->has('confirm_password')) <span class="label label-danger">{{ $errors->first('confirm_password') }}</span> @endif
                
                <div align="right">
                {!! Form::submit('Create my account', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent']) !!}
                </div>
                
                {!! Form::close() !!}
              
              
            </div>



          @section('footer')
          <footer class="demo-footer mdl-mini-footer">
          <div class="mdl-mini-footer--left-section">
            <ul class="mdl-mini-footer--link-list">
              <li><a href="#">Help</a></li>
              <li><a href="#">Privacy and Terms</a></li>
              <li><a href="#">User Agreement</a></li>
            </ul>
          </div>
        </footer>
          @stop
          @stop