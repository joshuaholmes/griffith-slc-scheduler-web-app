<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- This stuff is to support mobile web browsers -->
    <!-- Google -->
    <link rel="manifest" href="manifest.json">
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="icon" sizes="192x192" href="nice-highres.png">
    <!-- Apple -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    
    <!-- MDL CSS -->
    <link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.1/material.red-blue.min.css" />
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="/laravel/public/css/style.css">
    
    <!-- Google Analytics -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    
      ga('create', 'UA-67318620-1', 'auto');
      ga('send', 'pageview');
  
    </script>
    
    @yield('header')
  </head>
  
  <body>   
     <!-- Always shows a header, even in smaller screens. -->
    <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
      <header class="mdl-layout__header">
        <div class="mdl-layout__header-row">
          <!-- Title -->
          <span class="mdl-layout-title">Griffith SLC Scheduler</span>
          <!-- Add spacer, to align navigation to the right -->
          <div class="mdl-layout-spacer"></div>
          <!-- Navigation. We hide it in small screens. -->
          <nav class="mdl-navigation mdl-layout--large-screen-only">
            @if (Auth::check())
            <a class="mdl-navigation__link">Hi, {{Auth::user()->name}}</a>
            <a class="mdl-navigation__link" href="{{ URL::route('user.schedule')}}">Personal Schedule</a>
            @else
            <a class="mdl-navigation__link" href="{{ URL::route('user.signin')}}">Sign In</a>
            <a class="mdl-navigation__link" href="{{ URL::route('user.create')}}">Sign Up</a>
            @endif
            <a class="mdl-navigation__link" href="{{ URL::route('schedule.index')}}">Full Schedule</a>
          </nav>
        </div>
      </header>
      <div class="mdl-layout__drawer">
        <span class="mdl-layout-title">SLC Scheduler</span>
        <nav class="mdl-navigation">
          <a class="mdl-navigation__link" href="/">Home</a>
          <a class="mdl-navigation__link" href="{{ URL::route('schedule.index')}}">Full Schedule</a>
          @if (Auth::check())
          <a class="mdl-navigation__link" href="{{ URL::route('user.schedule')}}">Personal Schedule</a>
          <a class="mdl-navigation__link" href="{{ URL::route('user.logout')}}">Sign Out</a>
          @else
          <a class="mdl-navigation__link" href="{{ URL::route('user.signin')}}">Sign In</a>
          @endif
          <div class="mdl-layout-spacer"></div>
          <a class="mdl-navigation__link" href="/laravel/public/about">About</a>
          @if (Auth::check()) @if (Auth::user()->group == 'Admin') <a class="mdl-navigation__link" href="{{ URL::route('admin.index')}}">Admin</a> @endif @endif
        </nav>
      </div>
      
        
    
    <main class="mdl-layout__content">
  <div class="demo-container mdl-grid">
        
     @yield('main')
     </div>
    
    @yield('footer')

    </main>
     </div>
    @section('button')
    @show
    <!-- MDL Scripts -->
    <script src="https://storage.googleapis.com/code.getmdl.io/1.0.1/material.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  @yield('modal')
  </body>
  
  </html>