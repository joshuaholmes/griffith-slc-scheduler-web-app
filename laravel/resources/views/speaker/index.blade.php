@extends('master')

@section('header')
    <style>
    .demo-card-square.mdl-card {
  width: 320px;
  height: 320px;
}
.demo-card-square > .mdl-card__title {
  color: #fff;
  background:
    url('/laravel/public/{{$speaker->avatar->url('large')}}')
    bottom right 15% 
    no-repeat #46B6AC;
}
</style>
    
    @stop

@section('main')
<div class="mdl-cell mdl-cell--4-col mdl-cell--hide-tablet mdl-cell--hide-phone"></div>

<div class="demo-card-square mdl-card mdl-shadow--2dp">
  <div class="mdl-card__title mdl-card--expand">
    <h2 class="mdl-card__title-text">{{$speaker->name}}</h2>
  </div>
  <div class="mdl-card__supporting-text">
    {{$speaker->speciality}}<br>
     {{$speaker->bio}}<br>
     {{$speaker->contact}}
  </div>
  <div class="mdl-card__actions mdl-card--border">
    <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" onclick="goBack()">
      Go Back
    </a>
  </div>
</div>

<script>
function goBack() {
    window.history.back();
}
</script>
  

   
          @stop
          
@section('button')


@stop