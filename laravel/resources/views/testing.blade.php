<html>
    <head>
        <title>This page is used for testing</title>
        <link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>

        <style>
            body {
                margin: 0;
                padding: 0;
                width: 100%;
                height: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                margin-top: 100px;
                text-align: center;
                display: table-cell;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
                margin-bottom: 40px;
            }

            .quote {
                color: #222;
                text-align: left;
                width: 80%;
                margin: auto;
                font-size: 20px;
            }
            
            .quote .red {
                color: red;
                margin-right: 20px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Testing</div>
                <div class="quote">
                    <p><strong class="red">Users-example    : </strong> {{ $data['user'] }}</p>
                    <p><strong class="red">Speaker-example  : </strong> {{ $data['speaker'] }}</p>
                    <p><strong class="red">Activity-example : </strong> {{ $data['activity'] }}</p>
                    <p><strong class="red">Accessing M-M table : </strong> {{ $data['pivot'] }}</p>
                    <p><strong class="red">Saving M-M table : </strong> <br>$activity = new Activity; <br> $user  = User::find($id); <br> $user->activities()->save($activity);</p>
                </div>
            </div>
        </div>
    </body>
</html>
