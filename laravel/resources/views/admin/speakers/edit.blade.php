@extends('admin.master')

@section('title')
Speakers- Admin Portal
@stop

@section('heading')
Speakers
@stop

@section('nav')
<nav class="demo-navigation mdl-navigation mdl-color--blue-grey-800">
          <a class="mdl-navigation__link" href="{{ URL::route('admin.index')}}"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">home</i>Home</a>
          <a class="mdl-navigation__link" href="{{ URL::route('admin.users.index')}}"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">face</i>Users</a>
          <a class="mdl-navigation__link" href="{{ URL::route('admin.speakers.index')}}"><i class="mdl-color-text--grey-blue-400 material-icons" role="presentation">mic</i>Speakers</a>
          <a class="mdl-navigation__link" href="{{ URL::route('admin.activities.index')}}"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">event</i>Activities</a>
          <div class="mdl-layout-spacer"></div>
          <a class="mdl-navigation__link" href=""><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">home</i>Scheduler Home</a>
          <a class="mdl-navigation__link" href="mailto:bugs@slcgriffith.com.au?Subject=I%20found%20a%20bug"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">bug_report</i>Report bug</a>
        </nav>
@stop

@section('content')
<div class="mdl-cell mdl-cell--2-col mdl-cell--hide-tablet mdl-cell--hide-phone"></div>
<div class="demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--12-col">
            <h3>Add Speaker</h3>
            <p></p>
            {!! Form::model($speaker, array('method' => 'PUT', 'route' => array('speaker.update', $speaker->id), 'files' => true)); !!}
                @if ($errors->has())
                                            <div class="alert alert-danger">
                                                @foreach ($errors->all() as $error)
                                                    {{ $error }}<br>       
                                                @endforeach
                                            </div>
                                        @endif
                                        <div class="form-group">
                                            <div class="mdl-textfield mdl-js-textfield textfield-demo mdl-textfield--floating-label @if ($errors->has('name')) has-error @endif">
                                                {!! Form::text('name', null, array('class' => 'mdl-textfield__input')) !!}
                                                {!! Form::label('name', 'Name of speaker', array('class' => 'mdl-textfield__label')) !!}
                                            </div>
                                            </div>
                                        @if ($errors->has('name')) <span class="label label-danger">{{ $errors->first('name') }}</span> @endif
                                        <div class="form-group">
                                            <div class="mdl-textfield mdl-js-textfield textfield-demo mdl-textfield--floating-label @if ($errors->has('specialty')) has-error @endif">
                                                {!! Form::text('specialty', null, array('class' => 'mdl-textfield__input')) !!}
                                                {!! Form::label('specialty', 'Specialty or focus area if applicable', array('class' => 'mdl-textfield__label')) !!}
                                            </div>
                                            </div>
                                        @if ($errors->has('specialty')) <span class="label label-danger">{{ $errors->first('specialty') }}</span> @endif
                                        
                                        <div class="form-group">
                                            <div class="mdl-textfield mdl-js-textfield textfield-demo mdl-textfield--floating-label @if ($errors->has('bio')) has-error @endif">
                                                {!! Form::textarea('bio', null, array('class' => 'mdl-textfield__input', 'type' => 'text', 'rows' => '3')) !!}
                                                {!! Form::label('bio', 'Speaker bio', array('class' => 'mdl-textfield__label')) !!}
                                            </div>
                                            </div>
                                        @if ($errors->has('bio')) <span class="label label-danger">{{ $errors->first('bio') }}</span> @endif
                                        <div class="form-group">
                                                <div class="mdl-textfield mdl-js-textfield textfield-demo mdl-textfield--floating-label @if ($errors->has('contact')) has-error @endif">
                                                    {!! Form::text('contact', null, array('class' => 'mdl-textfield__input')) !!}
                                                    {!! Form::label('contact', 'Contact number or email if applicable', array('class' => 'mdl-textfield__label')) !!}
                                                </div>
                                                </div>
                                        @if ($errors->has('contact')) <span class="label label-danger">{{ $errors->first('contact') }}</span> @endif
                                        <div class="form-group">
                                            <div class="mdl-textfield mdl-js-textfield textfield-demo mdl-textfield--floating-label @if ($errors->has('image')) has-error @endif">
                                            
                                        {!! Form::label('image', 'Choose a speaker picture', array('class' => 'mdl-textfield__label')) !!}<br><br>
                                         {!! Form::file('image')!!}
                                        </div>
                                       
                                    </div>
               
                
                <div align="right">
                {!! Form::submit('Update speaker', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent']) !!}
                <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored" onclick="history.back(-1)">Cancel</button>
                </div>
                
                {!! Form::close() !!}
              
              
            </div>
            
@stop

@section('button')
@stop