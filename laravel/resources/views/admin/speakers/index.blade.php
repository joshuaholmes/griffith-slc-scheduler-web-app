@extends('admin.master')

@section('title')
Speakers- Admin Portal
@stop

@section('heading')
Speakers
@stop

@section('nav')
<nav class="demo-navigation mdl-navigation mdl-color--blue-grey-800">
          <a class="mdl-navigation__link" href="{{ URL::route('admin.index')}}"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">home</i>Home</a>
          <a class="mdl-navigation__link" href="{{ URL::route('admin.users.index')}}"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">face</i>Users</a>
          <a class="mdl-navigation__link" href="{{ URL::route('admin.speakers.index')}}"><i class="mdl-color-text--grey-blue-400 material-icons" role="presentation">mic</i>Speakers</a>
          <a class="mdl-navigation__link" href="{{ URL::route('admin.activities.index')}}"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">event</i>Activities</a>
          <div class="mdl-layout-spacer"></div>
          <a class="mdl-navigation__link" href=""><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">home</i>Scheduler Home</a>
          <a class="mdl-navigation__link" href="mailto:bugs@slcgriffith.com.au?Subject=I%20found%20a%20bug"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">bug_report</i>Report bug</a>
        </nav>
@stop

@section('content')
<div class="mdl-cell mdl-cell--2-col mdl-cell--hide-tablet mdl-cell--hide-phone"></div>
<div class="demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--12-col">
            <h3>Add Speaker</h3>
            <p></p>
            {!! Form::open(array('url' => secure_url('speaker'), 'files' => true)) !!}
                @if ($errors->has())
                                            <div class="alert alert-danger">
                                                @foreach ($errors->all() as $error)
                                                    {{ $error }}<br>       
                                                @endforeach
                                            </div>
                                        @endif
                                        <div class="form-group">
                                            <div class="mdl-textfield mdl-js-textfield textfield-demo mdl-textfield--floating-label @if ($errors->has('name')) has-error @endif">
                                                {!! Form::text('name', null, array('class' => 'mdl-textfield__input')) !!}
                                                {!! Form::label('name', 'Name of speaker', array('class' => 'mdl-textfield__label')) !!}
                                            </div>
                                            </div>
                                        @if ($errors->has('name')) <span class="label label-danger">{{ $errors->first('name') }}</span> @endif
                                        <div class="form-group">
                                            <div class="mdl-textfield mdl-js-textfield textfield-demo mdl-textfield--floating-label @if ($errors->has('speciality')) has-error @endif">
                                                {!! Form::text('speciality', null, array('class' => 'mdl-textfield__input')) !!}
                                                {!! Form::label('speciality', 'Speciality or focus area if applicable', array('class' => 'mdl-textfield__label')) !!}
                                            </div>
                                            </div>
                                        @if ($errors->has('speciality')) <span class="label label-danger">{{ $errors->first('speciality') }}</span> @endif
                                        
                                        <div class="form-group">
                                            <div class="mdl-textfield mdl-js-textfield textfield-demo mdl-textfield--floating-label @if ($errors->has('bio')) has-error @endif">
                                                {!! Form::textarea('bio', null, array('class' => 'mdl-textfield__input', 'type' => 'text', 'rows' => '3')) !!}
                                                {!! Form::label('bio', 'Speaker bio', array('class' => 'mdl-textfield__label')) !!}
                                            </div>
                                            </div>
                                        @if ($errors->has('bio')) <span class="label label-danger">{{ $errors->first('bio') }}</span> @endif
                                        <div class="form-group">
                                                <div class="mdl-textfield mdl-js-textfield textfield-demo mdl-textfield--floating-label @if ($errors->has('contact')) has-error @endif">
                                                    {!! Form::text('contact', null, array('class' => 'mdl-textfield__input')) !!}
                                                    {!! Form::label('contact', 'Contact number or email if applicable', array('class' => 'mdl-textfield__label')) !!}
                                                </div>
                                                </div>
                                        @if ($errors->has('contact')) <span class="label label-danger">{{ $errors->first('contact') }}</span> @endif
                                        <div class="form-group">
                                            <div class="mdl-textfield mdl-js-textfield textfield-demo mdl-textfield--floating-label @if ($errors->has('image')) has-error @endif">
                                             {!! Form::label('image', 'Choose a speaker picture', array('class' => 'mdl-textfield__label')) !!}<br><br>
                                         {!! Form::file('image')!!}
                                        </div>
                                       
                                    </div>
                @if ($errors->has('image')) <span class="label label-danger">{{ $errors->first('image') }}</span> @endif
                
                <div align="right">
                {!! Form::submit('Add speaker', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent']) !!}
                </div>
                
                {!! Form::close() !!}
              
              
            </div>

          <div class="demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--12-col">
            <h3>Current Speakers</h3>
            <p></p>
            <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp mdl-cell--12-col">
  <thead>
    <tr>
      <th class="mdl-data-table__cell--non-numeric">Name</th>
      <th>Specialty</th>
      <th>Bio</th>
      <th>Contact</th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
     @foreach($speakers as $speaker)
    <tr>
      <td class="mdl-data-table__cell--non-numeric">{{$speaker->name}}</td>
      <td>{{$speaker->specialty}}</td>
      <td>{{$speaker->bio}}</td>
      <td>{{$speaker->contact}}</td>
      <td><a href="{{{ url("admin/speakers/edit/$speaker->id") }}}" class="mdl-button mdl-js-button mdl-button--icon">
  <i class="material-icons">mode_edit</i>
</a></td>
<td><a href="{{{ url("speaker_delete/$speaker->id") }}}" class="mdl-button mdl-js-button mdl-button--icon mdl-button--colored">
  <i class="material-icons">delete</i>
</a></td>
    </tr>
    @endforeach
  </tbody>
</table>
            </div>
            
@stop

@section('button')
<a href="" id="view-source" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--accent mdl-color-text--accent-contrast">Print PDF</a>
@stop