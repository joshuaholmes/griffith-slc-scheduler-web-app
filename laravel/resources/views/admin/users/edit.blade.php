@extends('admin.master')

@section('title')
Users- Admin Portal
@stop

@section('heading')
Users
@stop

@section('nav')
<nav class="demo-navigation mdl-navigation mdl-color--blue-grey-800">
          <a class="mdl-navigation__link" href="{{ URL::route('admin.index')}}"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">home</i>Home</a>
          <a class="mdl-navigation__link" href="{{ URL::route('admin.users.index')}}"><i class="mdl-color-text--grey-blue-400 material-icons" role="presentation">face</i>Users</a>
          <a class="mdl-navigation__link" href="{{ URL::route('admin.speakers.index')}}"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">mic</i>Speakers</a>
          <a class="mdl-navigation__link" href="{{ URL::route('admin.activities.index')}}"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">event</i>Activities</a>
          <div class="mdl-layout-spacer"></div>
          <a class="mdl-navigation__link" href=""><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">home</i>Scheduler Home</a>
          <a class="mdl-navigation__link" href="mailto:bugs@slcgriffith.com.au?Subject=I%20found%20a%20bug"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">bug_report</i>Report bug</a>
        </nav>
@stop

@section('content')
<div class="mdl-cell mdl-cell--2-col mdl-cell--hide-tablet mdl-cell--hide-phone"></div>
<div class="demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--12-col">
            <h3>Edit User</h3>
            <p></p>
            {!! Form::model($profile, array('method' => 'PUT', 'route' => array('user.update', $profile->id))); !!}
                                    <div class="form-group">
                                        @if ($errors->has())
                                            <div class="alert alert-danger">
                                                @foreach ($errors->all() as $error)
                                                    {{ $error }}<br>       
                                                @endforeach
                                            </div>
                                        @endif
                                                <div class="mdl-textfield mdl-js-textfield textfield-demo mdl-textfield--floating-label @if ($errors->has('name')) has-error @endif">
                                                {!! Form::text('name', null, array('class' => 'mdl-textfield__input')) !!}
                                                {!! Form::label('name', 'Edit your name', array('class' => 'mdl-textfield__label')) !!}
                                            </div>
                                            @if ($errors->has('name')) <p class="alert alert-danger">{{ $errors->first('name') }}</p> @endif
                                    </div>
                                    <div class="form-group">
                                        
                                            <div class="mdl-textfield mdl-js-textfield textfield-demo mdl-textfield--floating-label @if ($errors->has('email')) has-error @endif">
                                                {!! Form::email('email', null, array('class' => 'mdl-textfield__input')) !!}
                                                {!! Form::label('email', 'Edit email', array('class' => 'mdl-textfield__label')) !!}
                                            </div>
                                        @if ($errors->has('email')) <p class="alert alert-danger">{{ $errors->first('email') }}</p> @endif
                                        
                                    </div>
                                    <div class="form-group">
                                        
                                            <div class="mdl-textfield mdl-js-textfield textfield-demo mdl-textfield--floating-label @if ($errors->has('password')) has-error @endif">
                                                {!! Form::password('password', array('class' => 'mdl-textfield__input')) !!}
                                                {!! Form::label('password', 'Change password', array('class' => 'mdl-textfield__label')) !!}
                                            </div>
                                        @if ($errors->has('password')) <p class="alert alert-danger">{{ $errors->first('password') }}</p> @endif
                                        
                                        </div>
                                        
                                    <div class="form-group">
                                        
                                            <div class="mdl-textfield mdl-js-textfield textfield-demo mdl-textfield--floating-label @if ($errors->has('group')) has-error @endif">
                                                {!! Form::select('group', array('User' => 'User', 'Super' => 'Super', 'Admin' => 'Admin'), $user->privacy, array('class' => 'mdl-textfield__input')) !!}
                                                {!! Form::label('group', 'Change permission level', array('class' => 'mdl-textfield__label')) !!}
                                        </div>
                                        @if ($errors->has('group')) <p class="alert alert-danger">{{ $errors->first('group') }}</p> @endif
                                    </div>
                                    <div align="right">
                                        {!! Form::submit('Update user', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent']) !!}
                                        <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored" onclick="history.back(-1)">Cancel</button>
                                        </div>
                                        {!! Form::close() !!}
                                        
            </div>
@stop