@extends('admin.master')

@section('title')
Users- Admin Portal
@stop

@section('heading')
Users
@stop

@section('nav')
<nav class="demo-navigation mdl-navigation mdl-color--blue-grey-800">
          <a class="mdl-navigation__link" href="{{ URL::route('admin.index')}}"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">home</i>Home</a>
          <a class="mdl-navigation__link" href="{{ URL::route('admin.users.index')}}"><i class="mdl-color-text--grey-blue-400 material-icons" role="presentation">face</i>Users</a>
          <a class="mdl-navigation__link" href="{{ URL::route('admin.speakers.index')}}"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">mic</i>Speakers</a>
          <a class="mdl-navigation__link" href="{{ URL::route('admin.activities.index')}}"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">event</i>Activities</a>
          <div class="mdl-layout-spacer"></div>
          <a class="mdl-navigation__link" href=""><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">home</i>Scheduler Home</a>
          <a class="mdl-navigation__link" href="mailto:bugs@slcgriffith.com.au?Subject=I%20found%20a%20bug"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">bug_report</i>Report bug</a>
        </nav>
@stop

@section('content')
<div class="mdl-cell mdl-cell--2-col mdl-cell--hide-tablet mdl-cell--hide-phone"></div>
<div class="demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--12-col">
            <h3>Current Users</h3>
            <p></p>
            <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp mdl-cell--12-col">
  <thead>
    <tr>
      <th class="mdl-data-table__cell--non-numeric">Name</th>
      <th>Email</th>
      <th>Group</th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
     @foreach($users as $user)
    <tr>
      <td class="mdl-data-table__cell--non-numeric">{{$user->name}}</td>
      <td>{{$user->email}}</td>
      <td>{{$user->group}}</td>
      <td><a href="{{{ url("admin/users/edit/$user->id") }}}" class="mdl-button mdl-js-button mdl-button--icon">
  <i class="material-icons">mode_edit</i>
</a></td>
<td><a href="{{{ url("user_delete/$user->id") }}}" class="mdl-button mdl-js-button mdl-button--icon mdl-button--colored">
  <i class="material-icons">delete</i>
</a></td>
    </tr>
    @endforeach
  </tbody>
</table>
            </div>
@stop