<!doctype html>
<!--
  Material Design Lite
  Copyright 2015 Google Inc. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      https://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License
-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- This stuff is to support mobile web browsers -->
    <!-- Google -->
    <link rel="manifest" href="manifest.json">
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="icon" sizes="192x192" href="nice-highres.png">
    <!-- Apple -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    
    <title>@yield('title')</title>
    
    <!-- Scripts for pdf printing -->
    <script src='/laravel/public/js/pdfmake.min.js'></script>
    <script src='/laravel/public/js/vfs_fonts.js'></script>

    <link href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.1/material.red-blue.min.css" />
    <link rel="stylesheet" href="/laravel/public/css/admin.css">
    <link rel="stylesheet" href="/laravel/public/css/style.css">
    <style>
    #view-source {
      position: fixed;
      display: block;
      right: 0;
      bottom: 0;
      margin-right: 40px;
      margin-bottom: 40px;
      z-index: 900;
    }
     
  .demo-card-wide > .mdl-card__title {
    color: #fff;
    height: 176px;
    background: url('https://lh3.googleusercontent.com/TT-dpPiR8U_EhkoHpglhLIylzqFgpovTi73RpDeLQzU=w533-h400-no') center / cover;
  }
  .demo-card-wide > .mdl-card__menu {
    color: #fff;
  }
    .demo-card-square.mdl-card {
    width: 320px;
    height: 320px;
  }
  .demo-card-square > .mdl-card__title {
    color: #fff;
    background:
      url('https://lh3.googleusercontent.com/tFRjq5nyrNmpSyF3UNb7cBkk1zDioKR7gcldcKSU_aw=w355-h215-no') bottom right 15% no-repeat #46B6AC;
  }
    </style>
  </head>
  <body>
    <div class="demo-layout mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
      <header class="demo-header mdl-layout__header mdl-color--white mdl-color--grey-100 mdl-color-text--grey-600">
        <div class="mdl-layout__header-row">
          <span class="mdl-layout-title">@yield('heading')</span>
          <div class="mdl-layout-spacer"></div>
        </div>
      </header>
      <div class="demo-drawer mdl-layout__drawer mdl-color--blue-grey-900 mdl-color-text--blue-grey-50">
        <header class="demo-drawer-header">
          <div class="demo-avatar-dropdown">
            @if (Auth::check())
            <span>{{Auth::user()->name}}</span>
            @endif
            <div class="mdl-layout-spacer"></div>
            <button id="accbtn" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">
              <i class="material-icons" role="presentation">arrow_drop_down</i>
              <span class="visuallyhidden">Accounts</span>
            </button>
            <ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" for="accbtn">
              <a href="{{{ url("user/logout") }}}" class="mdl-menu__item">Sign out</a>
            </ul>
          </div>
        </header>
        @section('nav')
          @show
        
      </div>
      <main class="mdl-layout__content mdl-color--grey-100">
        <div class="mdl-grid demo-content">
            
            @section('content')
            @show
            
            </div>
      </main>
    </div>
      
    
    @section('button')
    @show
    <script src="https://storage.googleapis.com/code.getmdl.io/1.0.1/material.min.js"></script>
  </body>
</html>