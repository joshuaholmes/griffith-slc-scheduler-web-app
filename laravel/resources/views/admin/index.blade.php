@extends('admin.master')

@section('title')
Griffith SLC Scheduler Admin Portal
@stop

@section('heading')
Home
@stop

@section('nav')
<nav class="demo-navigation mdl-navigation mdl-color--blue-grey-800">
          <a class="mdl-navigation__link" href="{{ URL::route('admin.index')}}"><i class="mdl-color-text--grey-blue-400 material-icons" role="presentation">home</i>Home</a>
          <a class="mdl-navigation__link" href="{{ URL::route('admin.users.index')}}"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">face</i>Users</a>
          <a class="mdl-navigation__link" href="{{ URL::route('admin.speakers.index')}}"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">mic</i>Speakers</a>
          <a class="mdl-navigation__link" href="{{ URL::route('admin.activities.index')}}"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">event</i>Activities</a>
          <div class="mdl-layout-spacer"></div>
          <a class="mdl-navigation__link" href=""><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">home</i>Scheduler Home</a>
          <a class="mdl-navigation__link" href="mailto:bugs@slcgriffith.com.au?Subject=I%20found%20a%20bug"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">bug_report</i>Report bug</a>
        </nav>
@stop

@section('content')
<div class="mdl-cell mdl-cell--2-col mdl-cell--hide-tablet mdl-cell--hide-phone"></div>

          <div class="demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--12-col">
            <h3>Welcome to the admin portal</h3>
            <p>This is where you can view users as well as add, remove and edit speakers and activities. A PDF printing option is coming soon.<br><br>
            Navigation is located to the left.</p>
            
            </div>
@stop