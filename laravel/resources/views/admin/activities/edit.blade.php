@extends('admin.master')

@section('title')
    Activities- Admin Portal
@stop

@section('heading')
    Activities
@stop

@section('nav')
    <nav class="demo-navigation mdl-navigation mdl-color--blue-grey-800">
        <a class="mdl-navigation__link" href="{{ URL::route('admin.index')}}"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">home</i>Home</a>
        <a class="mdl-navigation__link" href="{{ URL::route('admin.users.index')}}"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">face</i>Users</a>
        <a class="mdl-navigation__link" href="{{ URL::route('admin.speakers.index')}}"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">mic</i>Speakers</a>
        <a class="mdl-navigation__link" href="{{ URL::route('admin.activities.index')}}"><i class="mdl-color-text--grey-blue-400 material-icons" role="presentation">event</i>Activities</a>
        <div class="mdl-layout-spacer"></div>
        <a class="mdl-navigation__link" href=""><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">home</i>Scheduler Home</a>
        <a class="mdl-navigation__link" href="mailto:bugs@slcgriffith.com.au?Subject=I%20found%20a%20bug"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">bug_report</i>Report bug</a>
    </nav>
@stop

@section('content')
    <div class="mdl-cell mdl-cell--2-col mdl-cell--hide-tablet mdl-cell--hide-phone"></div>
    <div class="demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--12-col">
        <h3>Edit Activity</h3>
        {!! Form::model($activity, array('method' => 'PUT', 'route' => array('schedule.update', $activity->id))); !!}
            @if ($errors->has())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br>       
                    @endforeach
                </div>
            @endif
            <div class="form-group">
                <div class="mdl-textfield mdl-js-textfield textfield-demo mdl-textfield--floating-label mdl-textfield--floating-label @if ($errors->has('title')) has-error @endif">
                    {!! Form::text('title', null, array('class' => 'mdl-textfield__input')) !!}
                    {!! Form::label('title', 'Title of activity', array('class' => 'mdl-textfield__label')) !!}
                </div>
            </div>
            @if ($errors->has('title')) <span class="label label-danger">{{ $errors->first('title') }}</span> @endif
            <div class="form-group">
                <div class="mdl-textfield mdl-js-textfield textfield-demo mdl-textfield--floating-label mdl-textfield--floating-label @if ($errors->has('desc')) has-error @endif">
                    {!! Form::textarea('desc', null, array('class' => 'mdl-textfield__input', 'type' => 'text', 'rows' => '3')) !!}
                    {!! Form::label('desc', 'Description', array('class' => 'mdl-textfield__label')) !!}
                </div>
            </div>
            @if ($errors->has('desc')) <span class="label label-danger">{{ $errors->first('desc') }}</span> @endif
            <div class="form-group">
                <div class="mdl-textfield mdl-js-textfield textfield-demo mdl-textfield--floating-label mdl-textfield--floating-label @if ($errors->has('startingTime')) has-error @endif">
                    <div class="mdl-textfield mdl-js-textfield textfield-demo mdl-textfield--floating-label @if ($errors->has('time')) has-error @endif">
                        {!! Form::label('startingTime', 'Time slot', array('class' => 'mdl-textfield__label')) !!} <br><br>
                        {!! Form::select('startingTime', array('08:00am' => '08:00am', '09:00am' => '09:00am', '09:30am' => '09:30am', '10:30am' => '10:30am', '11:00am' => '11:00am', '12:30pm' => '12:30pm', '1:30pm' => '1:30pm', '2:30pm' => '2:30pm', '3:30pm' => '3:30pm', '4:30pm' => '4:30pm'), $activity->startingTime, array('class' => 'mdl-textfield__input')) !!}
                    </div>
                </div>
            </div>
            @if ($errors->has('startingTime')) <span class="label label-danger">{{ $errors->first('startingTime') }}</span> @endif
            <div class="form-group">
                <div class="mdl-textfield mdl-js-textfield textfield-demo mdl-textfield--floating-label mdl-textfield--floating-label @if ($errors->has('location')) has-error @endif">
                    {!! Form::text('location', null, array('class' => 'mdl-textfield__input')) !!}
                    {!! Form::label('location', 'Location of activity', array('class' => 'mdl-textfield__label')) !!}
                </div>
            </div>
            @if ($errors->has('location')) <span class="label label-danger">{{ $errors->first('location') }}</span> @endif
            <div class="form-group">
                <div class="mdl-textfield mdl-js-textfield textfield-demo mdl-textfield--floating-label mdl-textfield--floating-label @if ($errors->has('seatsAvailable')) has-error @endif">
                    {!! Form::text('seatsAvailable', null, array('class' => 'mdl-textfield__input')) !!}
                    {!! Form::label('seatsAvailable', 'Seats available', array('class' => 'mdl-textfield__label')) !!}
                </div>
            </div>
            @if ($errors->has('seatsAvailable')) <span class="label label-danger">{{ $errors->first('seatsAvailable') }}</span> @endif
            <div class="form-group">
                <div class="mdl-textfield mdl-js-textfield textfield-demo mdl-textfield--floating-label mdl-textfield--floating-label @if ($errors->has('speaker')) has-error @endif">
                    
                    @foreach($speakers as $s) 
                    
                    <?php
                        $data =  $activity->speakers()->get();
    		            $check = false;
                		foreach($data as $d)
                		{
                			if ($d['name'] ==  $s->name)
                                $check = true;                				
                		}
                    ?>
                        {!! Form::checkbox('speaker[]', $s->id, $check) !!} {{$s->name}} <br>
                        
                    @endforeach
                    
                    {!! Form::select('speakerOld', $speakers, $activity->speakers->first() ? $activity->speakers->first()->id : 0, array('class' => 'mdl-textfield__input', 'style' => 'opacity: 0;')) !!}
                    {!! Form::label('speakerLabel', 'Change speaker', array('class' => 'mdl-textfield__label')) !!}
                </div>
            </div>
            @if ($errors->has('speaker')) <span class="label label-danger">{{ $errors->first('speaker') }}</span> @endif
            <div align="right">
                {!! Form::submit('Update activity', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent']) !!}
                <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored" onclick="history.back(-1)">Cancel</button>
            </div>
        {!! Form::close() !!}
    </div>
    
    <div class="demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--12-col">
        <h3>Current Users</h3>
        <p></p>
        <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp mdl-cell--12-col">
            <thead>
                <tr>
                    <th class="mdl-data-table__cell--non-numeric">Name</th>
                    <th>Email</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $au)
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">{{$au->name}}</td>
                        <td>{{$au->email}}</td>
                        <td><a href="{{ url("activity_delete_user/$activity->id/$au->id") }}" class="mdl-button mdl-js-button mdl-button--icon mdl-button--colored">
                            <i class="material-icons">delete</i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@stop

@section('button')

    {!! Form::open(array('url' => url('admin/activities/printActivityUsers/'.$activity->id), 'method' => 'get', 'id' => 'pdfForm')) !!}
    {!! Form::submit('Print CSV', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--accent mdl-color-text--accent-contrast', 'id' => 'view-source','target' => '_blank' ]) !!}
    {!! Form::close() !!}
    
@stop