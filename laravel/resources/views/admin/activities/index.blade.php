@extends('admin.master')

@section('title')
    Activities- Admin Portal
@stop

@section('heading')
    Activities
@stop

@section('nav')
    <nav class="demo-navigation mdl-navigation mdl-color--blue-grey-800">
        <a class="mdl-navigation__link" href="{{ URL::route('admin.index')}}"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">home</i>Home</a>
        <a class="mdl-navigation__link" href="{{ URL::route('admin.users.index')}}"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">face</i>Users</a>
        <a class="mdl-navigation__link" href="{{ URL::route('admin.speakers.index')}}"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">mic</i>Speakers</a>
        <a class="mdl-navigation__link" href="{{ URL::route('admin.activities.index')}}"><i class="mdl-color-text--grey-blue-400 material-icons" role="presentation">event</i>Activities</a>
        <div class="mdl-layout-spacer"></div>
        <a class="mdl-navigation__link" href=""><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">home</i>Scheduler Home</a>
        <a class="mdl-navigation__link" href="mailto:bugs@slcgriffith.com.au?Subject=I%20found%20a%20bug"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">bug_report</i>Report bug</a>
    </nav>
@stop


@section('content')
    <div class="mdl-cell mdl-cell--2-col mdl-cell--hide-tablet mdl-cell--hide-phone"></div>
    <div class="demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--12-col">
        <h3>Add Activity</h3>
        <p></p>
        {!! Form::open(array('url' => secure_url('schedule'))) !!}
            @if ($errors->has())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br>       
                    @endforeach
                </div>
            @endif
            <div class="form-group">
                <div class="mdl-textfield mdl-js-textfield textfield-demo mdl-textfield--floating-label @if ($errors->has('title')) has-error @endif">
                    {!! Form::text('title', null, array('class' => 'mdl-textfield__input')) !!}
                    {!! Form::label('title', 'Title of activity', array('class' => 'mdl-textfield__label')) !!}
                </div>
            </div>
            @if ($errors->has('title')) <span class="label label-danger">{{ $errors->first('title') }}</span> @endif
            <div class="form-group">
                <div class="mdl-textfield mdl-js-textfield textfield-demo mdl-textfield--floating-label @if ($errors->has('description')) has-error @endif">
                    {!! Form::textarea('description', null, array('class' => 'mdl-textfield__input', 'type' => 'text', 'rows' => '3')) !!}
                    {!! Form::label('description', 'Description', array('class' => 'mdl-textfield__label')) !!}
                </div>
            </div>
            @if ($errors->has('description')) <span class="label label-danger">{{ $errors->first('description') }}</span> @endif
            <div class="form-group">
                <div class="mdl-textfield mdl-js-textfield textfield-demo mdl-textfield--floating-label @if ($errors->has('time')) has-error @endif">
                    {!! Form::label('time', 'Time slot', array('class' => 'mdl-textfield__label')) !!} <br><br>
                    {!! Form::select('time', array('08:00am' => '08:00am', '09:00am' => '09:00am', '09:30am' => '09:30am', '10:30am' => '10:30am', '11:00am' => '11:00am', '12:30pm' => '12:30pm', '1:30pm' => '1:30pm', '2:30pm' => '2:30pm', '3:30pm' => '3:30pm', '4:30pm' => '4:30pm'), array('class' => 'mdl-textfield__input')) !!}
                </div>
            </div>
            @if ($errors->has('time')) <span class="label label-danger">{{ $errors->first('time') }}</span> @endif
            <div class="form-group">
                <div class="mdl-textfield mdl-js-textfield textfield-demo mdl-textfield--floating-label @if ($errors->has('location')) has-error @endif">
                    {!! Form::text('location', null, array('class' => 'mdl-textfield__input')) !!}
                    {!! Form::label('location', 'Location of activity', array('class' => 'mdl-textfield__label')) !!}
                </div>
            </div>
            @if ($errors->has('location')) <span class="label label-danger">{{ $errors->first('location') }}</span> @endif
            <div class="form-group">
                <div class="mdl-textfield mdl-js-textfield textfield-demo mdl-textfield--floating-label @if ($errors->has('seats')) has-error @endif">
                    {!! Form::text('seats', null, array('class' => 'mdl-textfield__input')) !!}
                    {!! Form::label('seats', 'Seats available', array('class' => 'mdl-textfield__label')) !!}
                </div>
            </div>
            @if ($errors->has('seats')) <span class="label label-danger">{{ $errors->first('seats') }}</span> @endif
            <div align="right">
                {!! Form::submit('Add activity', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent']) !!}
            </div>
        {!! Form::close() !!}
    </div>
    <div class="demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--12-col" id="pdfHtml">
        <h3>Current Activities</h3>
        <p></p>
        <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp mdl-cell--12-col">
            <thead>
                <tr>
                    <th class="mdl-data-table__cell--non-numeric">Title</th>
                    <th>Description</th>
                    <th>Speaker</th>
                    <th>Time</th>
                    <th>Room</th>
                    <th>Seats</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($activities as $activity)
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">{{$activity->title}}</td>
                        <td>{{$activity->desc}}</td>
                        <td>@foreach ($activity['speakers'] as $key => $s) {{$s->name}} @if($key != count($activity['speakers'])-1)  ,<br>  @endif @endforeach</td>
                        <td>{{$activity->startingTime}}</td>
                        <td>{{$activity->location}}</td>
                        <td>{{$activity->seatsAvailable}}</td>
                        <td><a href="{{{ url("admin/activities/edit/$activity->id") }}}" class="mdl-button mdl-js-button mdl-button--icon">
                            <i class="material-icons">mode_edit</i>
                            </a>
                        </td>
                        <td><a href="{{{ url("activity_delete/$activity->id") }}}" class="mdl-button mdl-js-button mdl-button--icon mdl-button--colored">
                            <i class="material-icons">delete</i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@stop

@section('button')

    {!! Form::open(array('url' => url('admin/activities/printActivities'), 'method' => 'get', 'id' => 'pdfForm')) !!}
        {!! Form::hidden('pdfHtml', "old" , array('id' => 'pdfInput')) !!}
        {!! Form::submit('Print CSV', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--accent mdl-color-text--accent-contrast', 'id' => 'view-source', 'onclick' => 'printPdf2()', 'target' => '_blank' ]) !!}
{!! Form::close() !!}
    
    <!-- Using pdfMake 
    
      //  "barryvdh/laravel-dompdf": "0.6.*",
      //  "maatwebsite/excel": "~2.0",
      //  "symfony/symfony": "^3.0@dev"
    -->
    <script>
    
    // get the html content
    // set it to the input box
    
    
    function printPdf2() 
    {
        var html = document.getElementById("pdfHtml").innerHTML;
        document.getElementById("pdfInput").value = html;
    }
    </script>
@stop
