<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.1/material.red-blue.min.css" />
        <link rel="stylesheet" href="/laravel/public/css/style.css">
        <style>
        table {
            width: 100%;
        }
        
        </style>
    </head>
    <body>
        
        <h3>Griffith Student Leadership Conference</h3>
        <h5>Personal Schedule for {!! $user !!}</h5>
        <br>
        <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp mdl-cell--12-col full-width">
            <thead>
                <tr>
                  <th>Time</th>
                  <th class="mdl-data-table__cell--non-numeric full-width">Title</th>
                  <th>Speaker</th>
                  <th>Room</th>
            </thead>
            <tbody>
            @foreach($activities as $a)    
               <tr>
                  <td>{!! $a->startingTime !!}</td>
                  <td class="mdl-data-table__cell--non-numeric full-width">{!! $a->title !!}</td>
                  <td>@foreach ($a['speakers'] as $key => $s) {{$s->name}} @if($key != count($a['speakers'])-1)  ,<br>  @endif @endforeach</td>
                  <td>{!! $a->location !!}</td>
               </tr>
            @endforeach
                 
             </tbody>
    </table>
</body>
</html>